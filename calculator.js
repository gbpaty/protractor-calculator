let homepage = require('./page/homepage');
let urlProject = "http://juliemr.github.io/protractor-demo/";

describe('Iniciar calculadora angular demo', function() {

    it('It should have a title', function() {
      browser.get(urlProject.url);
      browser.driver.manage().window().maximize();
      homepage.verifyResultTitle('Super Calculator');
      browser.sleep(2000);
    });
    
    it('It should add', function() {
     homepage.getUrlProject(urlProject);
     homepage.enterFirstValueNumber('5');
     homepage.enterSecondValueNumber('3');
     homepage.clickButtonGo();
     browser.sleep(2000);
    });

    it('It should show result of add', function() {  
      homepage.verifyResult('8');
      browser.sleep(2000);
    });

  });